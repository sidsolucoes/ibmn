<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function ()     { return view('home'); });

Route::get('home', function () { return view('home'); });

Route::get('sobre', function () { return view('pages.sobre'); });

Route::get('missao', function () { return view('pages.missao'); });

Route::get('eventos', function () { return view('pages.eventos'); });

Route::get('noticias', function () { return view('pages.noticias'); });

Route::get('estudos', function () { return view('pages.estudos'); });

Route::get('downloads', function () { return view('pages.downloads'); });

Route::get('contato', function () { return view('pages.contato'); });

Route::get('players', function () { return view('pages.players'); });
