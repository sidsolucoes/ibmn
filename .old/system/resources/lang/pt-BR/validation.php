<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O :attribute deve ser aceito.',
    'active_url'           => 'O :attribute é uma URL válida.',
    'after'                => 'O :attribute deve ser uma data após :date.',
    'alpha'                => 'O :attribute só pode conter letras.',
    'alpha_dash'           => 'O :attribute só pode conter letras, números, e traços.',
    'alpha_num'            => 'O :attribute só pode conter letras e números.',
    'array'                => 'O :attribute deve ser um arry.',
    'before'               => 'O :attribute deve ser uma data antes de :date.',
    'between'              => [
        'numeric' => 'O :attribute deve ser entre :min e :max.',
        'file'    => 'O :attribute deve ser entre :min e :max kilobytes.',
        'string'  => 'O :attribute deve ser entre :min e :max caracteres.',
        'array'   => 'O :attribute deve ser entre :min e :max itens.',
    ],
    'boolean'              => 'O :attribute field must be true or false.',
    'confirmed'            => 'O :attribute confirmation does not match.',
    'date'                 => 'O :attribute is not a valid data.',
    'date_format'          => 'O :attribute does not match the format :format.',
    'different'            => 'O :attribute e :other must be different.',
    'digits'               => 'O :attribute must be :digits digits.',
    'digits_between'       => 'O :attribute deve ser entre :min e :max digits.',
    'dimensions'           => 'O :attribute has invalid image dimensions.',
    'distinct'             => 'O :attribute field has a duplicate value.',
    'email'                => 'O :attribute must be a valid email address.',
    'exists'               => 'O selected :attribute is invalid.',
    'file'                 => 'O :attribute must be a file.',
    'filled'               => 'O :attribute field is required.',
    'image'                => 'O :attribute must be an image.',
    'in'                   => 'O selected :attribute is invalid.',
    'in_array'             => 'O :attribute field does not exist in :other.',
    'integer'              => 'O :attribute must be an integer.',
    'ip'                   => 'O :attribute must be a valid IP address.',
    'json'                 => 'O :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'O :attribute não pode ser maior que :max.',
        'file'    => 'O :attribute não pode ser maior que :max kilobytes.',
        'string'  => 'O :attribute não pode ser maior que :max caracteres.',
        'array'   => 'O :attribute não pode ter mais que :max itens.',
    ],
    'mimes'                => 'O :attribute deve ser um arquivo do tipo: :values.',
    'mimetypes'            => 'O :attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O :attribute deve ter pelo menos :min.',
        'file'    => 'O :attribute deve ter pelo menos :min kilobytes.',
        'string'  => 'O :attribute deve ter pelo menos :min caracteres.',
        'array'   => 'O :attribute deve ter pelo menos :min itens.',
    ],
    'not_in'               => 'O selected :attribute is invalid.',
    'numeric'              => 'O :attribute must be a number.',
    'present'              => 'O :attribute field must be present.',
    'regex'                => 'O :attribute format is invalid.',
    'required'             => 'O :attribute campo é obrigatório.',
    'required_if'          => 'O :attribute campo é obrigatório when :other is :value.',
    'required_unless'      => 'O :attribute campo é obrigatório unless :other is in :values.',
    'required_with'        => 'O :attribute campo é obrigatório when :values is present.',
    'required_with_all'    => 'O :attribute campo é obrigatório when :values is present.',
    'required_without'     => 'O :attribute campo é obrigatório when :values is not present.',
    'required_without_all' => 'O :attribute campo é obrigatório when none of :values are present.',
    'same'                 => 'O :attribute e :other must match.',
    'size'                 => [
        'numeric' => 'O :attribute deve ter :size.',
        'file'    => 'O :attribute deve ter :size kilobytes.',
        'string'  => 'O :attribute deve ter :size caracteres.',
        'array'   => 'O :attribute must contain :size itens.',
    ],
    'string'               => 'O :attribute deve ter uma letra.',
    'timezone'             => 'O :attribute deve ter uma zona válida.',
    'unique'               => 'O :attribute has already been taken.',
    'uploaded'             => 'O :attribute failed to upload.',
    'url'                  => 'O :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
