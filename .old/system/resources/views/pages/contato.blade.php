@extends('layouts.master')

@section('content')
<section id="corpo">
    <section id="sobre">
        <div class="corpo">
            <div class="main">
                <div class="container">
                    <div class="col-md-3"></div>
                    <div class="formulario-contato col-md-6">
                        <h2>Contato</h2>
                        <form id="contato" action="{{ url('contato') }}">
                            <div class="cabecalho">
                                <div class="nome form-group row">
                                    <label for="nome" class="col-2 col-form-label">Nome:</label>
                                    <input name="nome" id="nome" type="text" value="" class="form-control" placeholder="* Nome completo" class="inputbox text validate[require[required_nome]]" maxlength="255" size="60">
                                </div>
                                <div class="email form-group row">
                                    <label for="nome" class="col-2 col-form-label">E-mail:</label>
                                    <input name="email" id="email" type="text" value="" placeholder="* contato@email.com" class="form-control">
                                </div>
                            </div>
                            <div class="titulo-mensagem form-group row">
                                <label for="mensagem" class="col-2 col-form-label">Mensagem:</label>
                            </div>
                            <div class="texto form-group row">
                                <textarea name="mensagem" id="mensagem" cols="30" rows="10" class="form-control" maxlength="20000" placeholder="Mensagem*"></textarea>
                            </div>
                            <div class="botao">
                                <input name="enviar" type="submit" value="Enviar" class="btn btn-success btn-x">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection