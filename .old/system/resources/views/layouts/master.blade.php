<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }} - {{ config('app.subtitle') }}</title>

    <!-- Bootstrap -->
    <link href="http://ibmn.com.br/system/public/css/bootstrap.min.css" rel="stylesheet">

    <!-- CSS site -->
    <link href="http://ibmn.com.br/system/public/css/site.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

</head>
<body>
    <div class="container">
        @include('layouts.partials.topo')
    </div>

    @include('layouts.partials.banner')

    @yield('content')

    @include('layouts.partials.rodape')
</body>
</html>
