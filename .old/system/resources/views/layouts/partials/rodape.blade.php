    <section id="rodape" class="container">
        <div class="rodape-bloco col-md-12">
            <div class="rod-coluna1 col-sm-12 col-md-5">
                <h2>MEDITAÇÕES</h2>
                <span class="txt-meditacoes">
                    Deus não está morto, Ele vive e reina!
                </span>
            </div>
            <div class="rod-coluna2 visible-md visible-lg col-md-7">
                <div class="fundo-pergaminho">
                    <h2>SALMOS 100</h2>
                    <span class="txt-versiculo">
                        1 - Celebrai com júbilo todos os moradores da terra.
                    </span>
                </div>
            </div>
        </div>
    </section>