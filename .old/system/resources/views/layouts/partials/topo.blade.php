        <section id="topo">
            <div class="logo-topo col-sm-12 col-md-3">
                <div class="logo">
                    <a href="/" title="" rel="home" class="logo img-responsive">
                        <img src="/image/logo.png">
                    </a>
                </div>
            </div>
            <div class="bloco-topo col-md-8">
                <div class="tab-2">
                    <div class="slogan visible-md visible-lg col-md-7">
                        <span>Só Jesus Cristo Salva</span>
                    </div>
                    <div class="bloco-ouvir col-sm-12 col-md-4">
                        <a href="{{ url('/players') }}">
                            <div class="botao-ouvir"></div>
                        </a>
                    </div>
                </div>
                <div class="clear"></div>
                <div id="menu-topo" class="collapse navbar-collapse col-md-12">
                    <nav class="navbar navbar-default">
                        <ul class="nav navbar-nav">
                            <li class="active item-menu item-menu-info"><a href="/">Home</a></li>
                            <li class="item-menu item-menu-sucess"><a href="{{ url('/sobre') }}">Sobre</a></li>
                            <li class="item-menu item-menu-missao"><a href="{{ url('missao') }}">Missão</a></li>
                            <li class="item-menu  item-menu-eventos"><a href="{{ url('eventos') }}">Eventos</a></li>
                            <li class="item-menu item-menu-noticias"><a href="{{ url('noticias') }}">Notícias</a></li>
                            <li class="item-menu item-menu-estudos"><a href="{{ url('estudos') }}">Estudos</a></li>
                            <li class="item-menu item-menu-info"><a href="{{ url('downloads') }}">Downloads</a></li>
                            <li class="item-menu  item-menu-contato"><a href="{{ url('contato') }}">Contato</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="barra-azul col-md-12"></div>
        </section>