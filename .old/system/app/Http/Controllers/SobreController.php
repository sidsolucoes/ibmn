<?php namespace App\Http\Controllers;

class SobreController extends Controller {
    /**
     * Sobre Controller
     *
     * Create a new controller instance.
     *
     * @return void
     *
     *******************
     * @return Response
     */
    public function index()
    {
        return view('pages.sobre');
    }
}