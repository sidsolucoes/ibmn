<footer class="site-footer">
    <div class="container">


      <div class="row">
        <div class="col-md-4">
          <h3 class="footer-heading mb-4 text-white">Sobre</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat quos rem ullam, placeat amet.</p>
          <p><a href="#" class="btn btn-primary pill text-white px-4">+ mais</a></p>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-6">
              <h3 class="footer-heading mb-4 text-white">Menu</h3>
                <ul class="list-unstyled">
                  <li><a href="#">Sobre</a></li>
                  <li><a href="#">Mensagens</a></li>
                  <li><a href="#">Eventos</a></li>
                  <li><a href="#">Contato</a></li>
                </ul>
            </div>
            <div class="col-md-6">
              <h3 class="footer-heading mb-4 text-white">Ministérios</h3>
                <ul class="list-unstyled">
                  <li><a href="#">Homens</a></li>
                  <li><a href="#">Mulheres</a></li>
                  <li><a href="#">A voz que clama</a></li>
                  <li><a href="#">Infantil</a></li>
                  <li><a href="#">Ada</a></li>
                  <li><a href="#">Teatro</a></li>
                  <li><a href="#">Coreografia</a></li>
                </ul>
            </div>
          </div>
        </div>


        <div class="col-md-2">
          <div class="col-md-12"><h3 class="footer-heading mb-4 text-white">Redes sociais</h3></div>
            <div class="col-md-12">
              <p>
                <a href="//bit.ly/FACEBOOK--IBMN" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                <a href="//bit.ly/INSTAGRAM--IBMN" class="p-2"><span class="icon-instagram"></span></a>
                <a href="//bit.ly/YOUTUBE--IBMN" class="p-2"><span class="icon-youtube"></span></a>

              </p>
            </div>
        </div>
      </div>
      <div class="row pt-5 mt-5 text-center">
        <div class="col-md-12">
          <p>
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          Copyright © <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> Todos os direitos reservados | <a href="https://sidsolucoes.com.br" target="_blank">SiD Soluções</a>
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </p>
        </div>

      </div>
    </div>
  </footer>
