<div class="py-5 quick-contact-info">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
            <div>
                <h2><span class="icon-room"></span> Endereço</h2>
                <p class="mb-0">Rua Sessenta e Seis, 46 <br>  Cohab V - Petrolina-PE-BR</p>
            </div>
            </div>
            <div class="col-md-4">
            <div>
                <h2><span class="icon-clock-o"></span> Cultos</h2>
                <p class="mb-0">Domingo 9:00h e às 19:00h <br>
                Terça-feira - 19:30h<br>
                Quinta-feira - 19:30h<br>
                Sábado - 19:30h Culto Jovem</p>
            </div>
            </div>
            <div class="col-md-4">
            <h2><span class="icon-comments"></span> contatos</h2>
            <p class="mb-0">Email: contato@ibmn.com.br <br>
            Fone: +55 (87) 9 9999-9999 </p>
            </div>
        </div>
    </div>
</div>
