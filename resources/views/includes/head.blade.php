<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="Saquib" content="Blade">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="author" content="{{ config('app.author') }}">
<meta name="robots" content="{{ config('app.robots') }}">

<title>{{ config('app.name') }} - {{ config('app.subtitle') }}</title>
<meta http-equiv="content-language" content="{{ app()->getLocale() }}">

<meta property="og:title" content="IBMN">
<meta property="og:description" content="Igreja Batista Missionária Nacional, um hambiente de adoração e família.">
<meta property="og:type" content="website">
<meta property="og:site_name" content="{{ config('app.name') }}">
<meta property="og:url" content="{{ config('app.url') }}">

<!-- load styles -->
<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
<link rel="stylesheet" href="fonts/icomoon/style.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<link rel="stylesheet" href="css/animate.css">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">

<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
<link rel="stylesheet" href="css/aos.css">
<link rel="stylesheet" href="css/style.css">
