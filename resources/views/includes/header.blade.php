<div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body">
        <ul class="site-nav-wrap">
            <li class="active">
              <a href="{{ URL::to('/') }}">Home</a>
            </li>
            <li><a href="#">Mensagens</a></li>
            <li><a href="events.html">Eventos</a></li>
            <li><a href="about.html">Sobre</a></li>
            <li><a href="contact.html">Contato</a></li>
        </ul>
    </div>
</div>
<!-- .site-mobile-menu -->
<div class="site-navbar-wrap js-site-navbar bg-white">
    <div class="container">
        <div class="site-navbar bg-light">
            <div class="py-1">
                <div class="row align-items-center">
                    <div class="col-2">
                    <h2 class="mb-0 site-logo"><a href="{{ URL::to('/') }}">IBMN</a></h2>
                    </div>
                    <div class="col-10">
                    <nav class="site-navigation text-right" role="navigation">
                        <div class="container">
                        <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

                        <ul class="site-menu js-clone-nav d-none d-lg-block">
                            <li class="active">
                            <a href="{{ URL::to('/') }}">Home</a>
                            </li>
                            <li><a href="#">Mensagens</a></li>
                            <li><a href="events.html">Eventos</a></li>
                            <li><a href="about.html">Sobre</a></li>
                            <li><a href="contact.html">Contatos</a></li>
                        </ul>
                        </div>
                    </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="height: 113px;"></div>
